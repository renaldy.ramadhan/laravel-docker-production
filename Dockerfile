FROM php:8.2-fpm as base

RUN apt update -y \
    && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    libonig-dev \
    libzip-dev \
    libpq-dev \
    libxml2-dev \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
    wget \
    gnupg \
    cron \
    supervisor \
    ca-certificates \
    && docker-php-ext-install bcmath ctype fileinfo \
    mbstring dom pdo pdo_pgsql pgsql mbstring zip exif pcntl gd

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

FROM base as app

WORKDIR /app

RUN groupadd -g 1000 application \
    && useradd -u 1000 -g application application

COPY --chown=application:application composer.json composer.lock /app/

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install --no-dev --no-scripts --no-autoloader --prefer-dist

COPY --chown=applicaation:application . .

RUN composer install --no-dev --prefer-dist

RUN chmod -R 777 /app/storage && \
    chmod -R 777 /app/bootstrap && \
    chown -R application:application /var/log/supervisor && \
    chmod -R 777 /var/log/supervisor

RUN php artisan event:cache && \
    php artisan route:cache && \
    php artisan view:cache

COPY container/supervisor/ /etc/

USER application

EXPOSE 9000

CMD /usr/bin/supervisord -c /etc/supervisor.conf

FROM nginxinc/nginx-unprivileged as web_server

WORKDIR /app

COPY container/nginx/nginx.conf.template /etc/nginx/templates/default.conf.template

COPY public /app/public

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
